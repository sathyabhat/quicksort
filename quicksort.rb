class QuickSort
  def sort(array)
    if array.length <= 1
      return array
    else
      pivot = array.sample
      array.delete_at(array.index(pivot))
      smaller_partition = []
      larger_partition = []
      array.each do |number|
        if number > pivot
          larger_partition << number
        else
          smaller_partition << number
        end
      end
    end
    sorted_array = []
    sorted_array << self.sort(larger_partition)
    sorted_array << pivot
    sorted_array << self.sort(smaller_partition)
    return sorted_array.flatten!
  end
end
