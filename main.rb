#!/bin/env ruby

require_relative 'quicksort'

number_of_elements = ARGV[0].to_i

if number_of_elements.nil? or number_of_elements == 0
    raise "Number of elements not passed/given as zero. Exiting."
end

qs = QuickSort.new
max_values = []
File.readlines("numbers.txt").each do |line| 
    max_values << line.split(', ').map(&:to_i)
    max_values.flatten!
end
max_numbers = qs.sort(max_values)
puts "Max values: #{max_numbers.slice(0,number_of_elements)}"
